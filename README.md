# AWS Ansible Provisioning

## Setup
1. Setup your AWS account and spin up a RHEL or CENTOS instance.
2. Create a AWS account access key Id and secret
3. With the access key and secret, set your environment variables like
```bash
export AWS_ACCESS_KEY_ID='......'
export AWS_SECRET_ACCESS_KEY='......'
```
4. Generate and save a keypair either via `ssh-keygen` or with AWS console. (remember to upload public key if using `ssh-keygen` method)
5. Change `private_key_file` entry in `ansible.cfg` to use the private key file generate on step 4.
6. `virtualenv venv` and `source venv/bin/activate` and `pip install -r requirements.txt`

## Test
*IMPORTANT* To tag your instance accordingly. I have mine as `tag_blog_instance_1` which is reflected in site.yml as hosts
1. Run `inventory/ec2.py --list`. Should return information of your EC2. (remember to chmod +x inventory/ec2.py)
2. Run `ansible -m ping tag_blog_instance_1`. Should response with "pong"

## Provisioning
1. Update `site.yml` with tasks or roles if you have one. This project uses role soondee.me which depends on common, hugo, nginx (See dir roles)
2. Run `ansible-playbook -vvvv site.yml`

## Local testing
I have included a Vagrantfile and a local inventory for local testing.
1. `vagrant up`
2. `source venv/bin/activate`
3. Update ansible.cfg for `inventory=./local`
4. `ansible-playbook -vvvv site.yml`

*NOTES*
For CentOS, `libselinux-python` must be installed on remote VMs if Selinux is enabled on the VMs.
